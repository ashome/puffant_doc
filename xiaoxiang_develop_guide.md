## 小象网开发文档
author :陈璟

### 项目结构  

<img src= "/image/xiaoxiang1.png" style="width:40%">  
admin.xiaoxiang 及 mkt.xiaoxiang 加上项目本身，分为三个不同的界面（用户使用，产品管理，后台管理）

1. #### app 文件夹   

  <img src= "/image/xiaoxiang2.png" style="width:40%">  
  Ruby On Rails 项目都应用了mvc 架构：  

  此文件夹中的 assets 文件夹中包含了网页所用到的 css，image,javascript,文件，用来控制页面显示
  controller（重点） 文件夹中包含着一系列的方法，由 routes文件夹告诉程序具体走那个方法，为架构中的 C ，也就是控制层  
  helper 辅助方法  
  mailers 邮件通知  
  models（重点）,这里存放着对于数据库操作的一些方法，比如数据库的关系，数据库的某种调用方法，所有有关数据库操作都应该在此完成，架构中的 M 成，也就是操作层
  view （重点），这里存放着用户看到的界面，架构中的 V，也就是显示层  
  总的来说 controller 控制 models 返回相应的数据，再传给 view 来展现，这就是 mvc 的基本流程
2. #### bin 文件夹  

  项目初始化的时候，此文件夹就已经生成，放置着项目用到的一些基本命令
3. #### config 文件夹  

  <img src= "/image/xiaoxiang3.png" style="width:40%">  
  * environments 为项目环境，development 为开发环境，production 为线上环境，test为测试环境，这个地方可以自定义环境
  * initializers 项目在运行的时候需要初始化的一些文件就需要放在这里，比如七牛的sdk 就在此完成初始化后整个项目就能随便调用
  * locales 在这个目录中放置这一些国际化的配置，i18n 配置
  * application.rb  项目的主入口，保存了一些项目初始设置，可以在此规定整个项目所处的时区，所用的语言，同时也可以通过字段来控制显示不同的界面，使用字段控制显示不同的界面就是由此文件控制
  * database.yml 数据库应用的配置文件，确定了数据库使用多少缓冲池，使用的数据库等等信息

4. #### db 文件夹  

  <img src= "/image/xiaoxiang4.png" style="width:40%">  
  * migrate  保存着命令生成数据表，修改，删除数据表的操作，也是数据库表操作控制的地方
  * schema  保存着整个数据库的所有表及字段
  * seed  保存着需要初始插入的数据，比如创建管理员账号
5. #### lib文件夹  

  可以放置着一些需要引用的第三方库
6. #### public 文件夹    

  所有外部能访问的资源都是放置在此文件夹下
7. #### Gemfile  

  使用的一些第三方 gem，如：devise，alipay，qiniu，redis，sass，coffee，puma，rails 等等


### 模块描述
1. #### 用户模块描述  

  整个用户数据表分为Account，AccountOauth，OauthUser, User  
  用户注册，分为两种情况：邮箱注册，第三方授权（微博，豆瓣，Instagram）。邮箱注册为直接注册user（Devise 带注册功能），第三授权则使用OmniAuth 进行第三方授权登陆功能，可以通过 rake routes 查看路由得知授权发出或者受到回调相对应哪个 method  
  用户登录后可以查询自己的印品，订单，及站内信  

2. #### 印品及订单  
  整个模块使用的表：Product，Bundle，Deck，Order  
  印品表：Deck，用户在选择印品时，选择完毕后印品创建出，当用户选择印品上传好图片后，一起提交，会生成一个订单（Order）编辑器会通过印品所选择的套餐（Bundle,套餐根据 product 分辨具体是什么事物）及套餐所使用的模板（Scaffold）来让用户上传图片  
  当用户印品已经创建完毕，用户点击提交订单。  
  Order 创建页面会让用户确定使用的快递信息，计算邮资，及是否使用优惠码等  
  Order会去执行 before_save，生成订单号等一系列操作  

3. #### 后台  

  1. ##### 账号管理模块  

    管理人员通过此模块查看用户信息，用户的待完成的印品或订单，修改用户未完成订单的信息.用户的已完成订单
  2. ##### 印品及订单管理  

    管理人员通过此模块查看，修改单个订单或者印品的信息
  3. ##### 优惠码模块  

    管理人员可以创建，修改，删除优惠码（用户可以通过发微博生成优惠码）。优惠码在使用一次后就会被删除
  4. ##### 公告模块  

    数据表为broadcasts，管理员可在相关页面对公告进行增删改操作
  5. ##### 产品模块  

    数据表为 products，管路员可在相关页面以进行产品进行增删改操作
  6. ##### 套餐模块  

    数据表为 bundles，管理员可以对其进行增删改查，这里还关系到 editor 中的scaffold 表
  7. ##### 首页幻灯片  

    数据表为product_slides,管理员可在相关页面设定在首页banner所展示的幻灯片，位置及关联的产品


### 小象网数据库关系图   

  ![xiaoxiang db](/image/xiaoxiangdb.png)  
