## 项目通用描述

### 开发环境描述
  1. 开发环境： ruby 版本：2.2.1以上(可使用 rbenv或RVM 安装 ruby 版本)，rails 版本 4.2以上
  2. 必要工具：bundle、pow、MariaDB、redis、bower(nodejs)
  3. 本地运行方式： MacOSX使用 brew 安装 pow ，按照步骤安装完成后会在 ~目录生成  .Pow 文件夹，cd 至文件夹内，使用 ln –s 命令引入。如 `ln -s ~/editor ~/.pow`。引入完成后在浏览器中输入 editor.dev 则可进入本地程序。` 本地数据库需要设置为无密码连接`   
  `注：`某些项目中分为不同管理界面，如 xiaoxiang 项目，此项目中包含：前端用户界面，后台产品管理界面，后台订单管理界面。
  此类界面通过一个全局变量控制，如xiaoxiang 项目中的 admin.xiaoxiang 文件夹，其中包含 tmp文件夹及 config.ru 文件，其中 config.ru 文件中第2行 ENV['PF_MODE'] = 'ADMIN' 这个使用1.3方法中引入到 ~/.pow 文件夹下，在浏览器中输入 admin.xiaoxiang.dev则可见到后台订单管理界面。所有项目沿用此规则。

  4. 部署：采用服务器自动配置，只需将最新的代码更新至 release 分支上，服务器会自动部署，服务器说使用的配置文件在 puffant项目中

### 服务器信息
  1. sz 服务器 ：位于龙岗，用作运行各种程序 server，项目源码运行的位置
  2. sz2 服务器 ：位于龙岗，此服务器配置极强，用于数据同步，文件处理，及项目中的 background_job（后台程序）
  3. ns 服务器 ： 位于南山办公室，用作南山办公室中的网络代理
  4. qcl 服务器：备用外线服务器，运行着所有项目
  5. zh 服务器 ：中华服务器，中华印刷厂接受印刷使用
  6. 线上所有数据都存储在此，在sz2服务器上映射。sz2服务器中的 mysql 为 staging 项目所使用的数据库,操作此数据不会影响线上数据
  7. 七牛云存储 ：用户存储用户上传的图片等静态资源文件

### 项目描述  
  这里简单描述所有代码的功能作用，在 github 中存放着所有的项目,master分支为开发分支，release 分支为项目部署分支，使用何版本（非代码版本）由项目全局变量控制。

  1. #### xiaoxiang  

  小象网项目，小象网相关的代码全在此项目中
  2. #### merrypapery  

  纸悦项目，纸悦相关代码全在此项目中
  3. #### editor  

  小象网项目，小象网相关的代码全在此项目中
  4. #### mhub  

  此项目为数据导出中心，可设定导出纸悦及小象的销售订单信息
  5. #### `plib(重要项目)`  

  此项目为公共工具包，包含着所有项目所用到的共有方法，以 gem 的方式存在，修改请慎重
  6. #### `puffant(重要项目)`  

  此项目包含服务器信息，部署配置等信息，修改请慎重
  7. #### pcenter  

  此项目为确定订单—>发货流程的项目
  8. #### opcenter  

  此项目为后台门户网站，控制后台人员权限
  9. #### singlecart  

  此项目为内部订单管理中心，查看订单所用（小象，纸悦，BlanchePrints）
  10. #### upcenter  

  此项目为文件管理
